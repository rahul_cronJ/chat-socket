const express = require('express');
const app = express();
const http =require('http').Server(app);

const io = require('socket.io')(http);

http.listen(3000,function() {
  console.log("server started on port:3000");
});

app.use(express.static('../pub'));

app.get('/',function(req,res){
  res.sendFile(__dirName + 'index.html');
});
var userList =[];
var groupList=[];
var publicChat = io.of('/public');
publicChat.on('connection',function(socket) {
  socket.on("chat-message",function(messageObj) {
    messageObj.name = socket.username;
    messageObj.from = socket.id;
    publicChat.to(messageObj.sendToSocket).emit('chat-message',messageObj);
  });
  var currentListenUser;
  socket.on("new-user",function(newUser) {
    console.log(newUser+" connected : "+socket.id);
    socket.username = newUser;
    var user = {
      name:newUser,
      socketId:socket.id,
    };
    userList.push(user);
    publicChat.emit('user-list',userList);
    publicChat.emit('group-list',groupList);
    console.log("total:"+userList.length);
  });
  socket.on("group-list",function(newGroup) {
    console.log(newGroup+" connected : "+socket.id);
    // socket.gr = newUser;
    var group = {
      name:newGroup,
      socketId:newGroup,
    };
    groupList.push(group);
    publicChat.emit('group-list',groupList);
    console.log("total:"+userList.length);
  });
  socket.on("join-room",function(room) {
      socket.join(room);
      console.log(socket.username +" joined : "+room);
  });

  socket.on('disconnect',function(data) {
    console.log('disconnected :'+data+":"+socket.username);
    userList.splice(userList.indexOf(socket.username),1);
    publicChat.emit('user-list',userList);
  });

  // socket.on("room-a",function(message) {
  //   console.log("room-a:");
  //   var mssg = {name:socket.username,msg:message};
  //   publicChat.emit("room-a",mssg);
  // });
})



app.all(/./,function(req,res) {
  res.send("Bad request");
});
