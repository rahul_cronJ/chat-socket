app.controller('ChatController',["$stateParams","$rootScope","$scope","SocketService",function($stateParams,$rootScope,$scope,SocketService) {
  console.log("ChatController");
  var username = $stateParams.user;
  console.log("user : "+username);
  var socket = io('/public');//
  socket.emit("new-user",username);//
  var currentEvent = "chat-message";
  $rootScope.userList =[];
  var currentEventUser={};
  this.messagebox = true;
  this.groups = [];
  this.countObj={};
  this.activeuser = "User";
  var $chatDiv = $('#chatmessage');
  socket.on("chat-message",function (message) {
      console.log(message);
      if((message.type == currentEvent && message.name!=username) && (message.from==currentEventUser.socketId || message.sendToSocket==currentRoom))
        $chatDiv.append('<div class="row"><p class="alert alert-success col-md-8"><b>'+message.name+': </b>'+message.msg+'</p></div>');

      $scope.$apply(function () {
        if(message.type == "ROOM")
            thisRef.countObj[message.sendToSocket] += 1;
        else
            thisRef.countObj[message.from] += 1;
      });
  });

  var thisRef = this;
  socket.on("user-list",function(getList) {
    $rootScope.userList = getList;
    $rootScope.$apply();
    $scope.$apply(function () {
      for( user in getList){
        if(!thisRef.countObj[getList[user].socketId])
          thisRef.countObj[getList[user].socketId]=0
      }
    });
    console.log("got list"+getList.length);
  });
  this.joinRoom = function(group) {
    console.log("room: "+group.name);
    $chatDiv.empty();
    currentEvent = "ROOM";
    currentRoom = group.name;
    currentEventUser.socketId =group.name;
    socket.emit("join-room",group.name);
    this.messagebox = false;
    this.countObj[group.socketId] = 0;
    this.activeuser = currentRoom;
  }
  this.sendMessage = function() {
    console.log("sending message");
    var msgOb = {
      type:currentEvent,
      sendToSocket:currentEventUser.socketId,
      msg:this.message
    };
    socket.emit("chat-message",msgOb);
    $chatDiv.append('<div class="row" ><p class="alert alert-info col-md-8 col-md-offset-4"><b>'+username+': </b>'+this.message+'</p></div>');
  }
  this.chatOnUser = function(user) {
    console.log("user:"+user);
    $chatDiv.empty();
    currentEvent = "USER";
    currentEventUser.socketId = user.socketId;
    this.messagebox = false;
    this.countObj[user.socketId] = 0;
    this.activeuser = user.name;
  }

  this.createGroup = function() {
    var group = this.groupname;
    socket.emit("group-list",group);
  }
  socket.on("group-list",function(getList) {
    $scope.$apply(function(){
      thisRef.groups = getList;
      console.log("got group list"+getList.length);
      for( user in getList){
          if(!thisRef.countObj[getList[user].socketId])
            thisRef.countObj[getList[user].socketId]=0
      }
    });
  });
  SocketService.dynaminc(function(data) {
    console.log(data);
  });
  SocketService.abcd =99;
}]);
