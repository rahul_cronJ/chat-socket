
var app = angular.module("socketchat",['ui.router']);
app.config(["$stateProvider","$locationProvider",function($stateProvider,$locationProvider) {
  $stateProvider
    .state("home",{
      url:'/home',
      templateUrl:'./templates/login.html',
      controller:'HomeController',
      controllerAs:"homeCtrl"
    })
    .state("chatroom",{
      url:'chatroom?user',
      templateUrl:'./templates/chat.html',
      controller:'ChatController',
      controllerAs:'chatCtrl'
    });
    $locationProvider.html5Mode(true);
}]);
